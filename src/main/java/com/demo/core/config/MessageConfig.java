package com.demo.core.config;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.MessageSource;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Conditional;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.support.ReloadableResourceBundleMessageSource;
import org.springframework.web.servlet.LocaleResolver;
import org.springframework.web.servlet.i18n.SessionLocaleResolver;

import com.demo.core.message.Labels;
//@Configuration
public class MessageConfig {
	  /** The encoding. */
    @Value("${spring.messages.encoding}")
    private String encoding;

    /** The base name. */
    @Value("${spring.messages.basename}")
    private String baseName;

    /** The cache duration. */
    @Value("${spring.messages.cache-duration}")
    private int cacheDuration;

    /** The use code as default message. */
    @Value("${spring.messages.use-code-as-default-message}")
    private boolean useCodeAsDefaultMessage;

    /**
     * Bundle message source.
     *
     * @return the message source
     */
    @Bean
    public MessageSource bundleMessageSource() {
        ReloadableResourceBundleMessageSource messageSource = new ReloadableResourceBundleMessageSource();
        messageSource.setBasename(this.baseName);
        messageSource.setDefaultEncoding(this.encoding);
        messageSource.setCacheSeconds(this.cacheDuration);
        messageSource.setUseCodeAsDefaultMessage(this.useCodeAsDefaultMessage);

        return messageSource;
    }

    /**
     * Locale resolver.
     *
     * @return the locale resolver
     */
    @Bean
    public LocaleResolver localeResolver() {
        SessionLocaleResolver localeResolver = new SessionLocaleResolver();

        localeResolver.setDefaultLocale(Labels.VN);

        return localeResolver;
    }
}
