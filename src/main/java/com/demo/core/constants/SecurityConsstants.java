package com.demo.core.constants;

public class SecurityConsstants {
    /**
     * The Interface Account.
     */
    public interface Account {

        /**
         * The system.
         */
        String SYSTEM = "system";
    }

    public static final long JWTEXPỈATIONINMS = 6_400_000;

    public static final long REFRESH_JWTEXPỈATIONINMS = 186_400_000;

    /**
     * The Interface Jwt.
     */
    public interface Jwt {

        /**
         * The authorization header.
         */
        String AUTHORIZATION_HEADER = "Authorization";

        /**
         * The token start.
         */
        String TOKEN_START = "Bearer ";

        /**
         * The privileges.
         */
        String PRIVILEGES = "privileges";

        /**
         * The hashkey.
         */
        String HASHKEY = "hash-key";

        /**
         * The refresh token.
         */
        String REFRESH_TOKEN = "refresh-token";

        /**
         * The user detail.
         */
        String USER_DETAIL = "user-detail";

        /**
         * The microsoft access token.
         */
        String MICROSOFT_ACCESS_TOKEN = "microsoft-access-token";
    }

    /**
     * The Interface SystemRole.
     */
    public interface SystemRole {

        /**
         * The admin.
         */
        String ADMIN = "ADMIN";

        /**
         * The user.
         */
        String USER = "USER";

        /**
         * The anonymous.
         */
        String ANONYMOUS = "ANONYMOUS";

        /**
         * The super admin.
         */
        String SUPER_ADMIN = "SUPER_ADMIN";
    }
}
