package com.demo.core.security.handler;

import java.io.IOException;
import java.time.Instant;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.web.authentication.AuthenticationFailureHandler;
import org.springframework.stereotype.Component;

import com.demo.entity.UserLogin;
import com.demo.service.UserLoginService;
@Component
public class AuthenticationFailureHandlerImpl implements  AuthenticationFailureHandler {
	  /** The user login service. */
    @Autowired
    private UserLoginService userLoginService;

	@Override
	public void onAuthenticationFailure(HttpServletRequest request, HttpServletResponse response,
		      AuthenticationException exception) throws IOException, ServletException {
        // Save authentication failure to user login table
        UserLogin loginLog = UserLogin.builder().username(request.getUserPrincipal().getName())
                .ip(request.getRemoteAddr()).loginTime(Instant.now()).success(false).description(exception.getMessage())
                .build();

        this.userLoginService.save(loginLog);
		
	}
}
