package com.demo.core.message;

public interface LabelKey {
    /** The system successful. */
    String SYSTEM_SUCCESSFUL = "system.success";

    /** The sytem failed. */
    String SYTEM_FAILED = "system.failed";

    /** The error action invalid. */
    String ERROR_ACTION_INVALID = "error.action.invalid";

    /** The error invalid token. */
    String ERROR_INVALID_TOKEN = "error.token.invalid";

    /** The error login input invalid. */
    String ERROR_LOGIN_INPUT_INVALID = "error.login.input.invalid";

    /** The error invalid username. */
    String ERROR_INVALID_USERNAME = "error.invalid.username";

    /** The error invalid user or password. */
    String ERROR_INVALID_USER_OR_PASSWORD = "error.invalid.username.or.password";

    /** The error constraint violation. */
    String ERROR_CONSTRAINT_VIOLATION = "error.constraint.violation";

    /** The error method argument not valid. */
    String ERROR_METHOD_ARGUMENT_NOT_VALID = "error.method.argument.not.valid";

    /** The error concurrency failure. */
    String ERROR_CONCURRENCY_FAILURE = "error.concurrency.failure";

    /** The login successful. */
    String LOGIN_SUCCESSFUL = "login.successful";

    /** The error input invalid. */
    String ERROR_INPUT_INVALID = "error.input.invalid";

    /** The error role code duplicate. */
    String ERROR_ROLE_CODE_DUPLICATE = "error.role.code.duplicate";

    /** The delete email false. */
    String DELETE_EMAIL_FALSE = "error.delete.email.false";

    /** The email id not exist. */
    String EMAIL_ID_NOT_EXIST = "error.emailId.not.existed";

    /** The error code data is existed. */
    String ERROR_CODE_DATA_IS_EXISTED = "error.code.data.is.existed";

    /** The error can not delete approval type. */
    String ERROR_CAN_NOT_DELETE_APPROVAL_TYPE = "error.can.not.delete.approval.type";

    /** The error can not delete work flow. */
    String ERROR_CAN_NOT_DELETE_WORK_FLOW = "error.can.not.delete.workflow";

    /** The error file storage failed. */
    String ERROR_FILE_STORAGE_FAILED = "error.file.storage.failed";;

    /** The mount is invalid. */
    String MOUNT_IS_INVALID = "error.mount.invalid";

    /** The error department invalid. */
    String ERROR_DEPARTMENT_INVALID = "error.department.invalid";

    /** The error department not exist. */
    String ERROR_DEPARTMENT_NOT_EXIST = "error.department.not.existed";

    /** The error record is waiting. */
    String ERROR_RECORD_IS_WAITING = "error.record.is.waiting";

    /** The department not summarize. */
    String DEPARTMENT_NOT_SUMMARIZE = "department.not.sumarize";

    /** The ci duplicated. */
    String CI_DUPLICATED = "error.ci.duplicated";

    /** The user current does not have permission. */
    String USER_CURRENT_DOES_NOT_HAVE_PERMISSION = "error.user.current.does.not.have.permission";

    /** The step user is expired. */
    String STEP_USER_IS_EXPIRED = "error.step.user.is.expired";

    /** The error can not delete budget plan. */
    String ERROR_CAN_NOT_DELETE_BUDGET_PLAN = "error.can.not.delete.budget.plan";

    /** The error criteria not exist*/
    String ERR_CRITERIA_NOT_EXIST = "error.criteria.not.exist";

    /** The user can not be edit*/
    String ERROR_CAN_NOT_EDIT_THIS_USER = "error.can.not.update.this.user";

    /** Update user successful*/
    String UPDATE_USER_SUCCESSFUL = "update.user.successful";

    /** Error user not exist*/
    String ERR_USER_NOT_EXIST = "error.user.not.exist";

    /** The error not exist. */
    String ERR_NOT_EXIST = "error.not.existed";

    /** The error is null. */
    String ERR_IS_NULL = "error.is.null";

    /** The error invalid data */
    String ERROR_DATA_INVALID = "error.data-invalid";

    /** The record does not exist */
    String ERROR_RECORD_DOES_NOT_EXIT = "error.record-does-not-exist";

    /** The error user not permission. */
    String ERR_USER_NOT_PERMISSION = "error.user.not.permission";

    /**
     * supplier evaluate error
     */
    String ERROR_SUPPLIER_EVALUATE_SCORE_INVALID = "error.sqm.supplier.evaluate-score-invalid";

    String ERROR_SUPPLIER_NOT_EXIST= "error.sqm.supplier.not-exist";

    String ERROR_ACCESSORY_NOT_EXIST= "error.sqm.accessory.not-exist";

    String ERROR_CRITERIA_NOT_EXIST= "error.sqm.criteria.not-exist";

    /**
     * Rating type error
     */
    String ERROR_RATING_TYPE_NOT_EXIST= "error.sqm.rating-type.not-exist";

    /** The error user not have email */
    String USER_NOT_HAVE_EMAIL = "error.not.have.email";

    /**
     * Ranking error
     */
    String ERROR_RANKING_LOT_NG_BIGGER_LOT = "error.sqm.ranking.lot-ng-bigger-lot";

    String ERROR_RANKING_IMPROVE_QPN_BIGGER_TOTAL_QPN = "error.sqm.ranking.improve-qpn-bigger-total-qpn";

    String ERROR_RANKING_FEEDBACK_TWENTY_FOUR_BIGGER_TOTAL_FEEDBACK = "error.sqm.ranking.feedback-twenty-four-bigger-total-feedback";

    // bản đánh giá xếp hạng tháng đã tồn tại -> 1 tháng chỉ có 1 bản đánh giá
    String ERROR_MONTHLY_RANKING_ALREADY_EXISTS = "error.sqm.ranking.monthly-ranking-already-exists";

    /**
     * Vendor error
     */
    String ERROR_VENDOR_DOES_NOT_EXIT = "error.sqm.vendor.not-exist";

    String ERROR_BADREQUEST = "error.badrequest";

}
