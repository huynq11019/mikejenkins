package com.demo.repository.custom;

public interface AccessTokenRepositoryCustom {
    int expiredTokenByUsername(String username);
}
