package com.demo.repository.custom;

import com.demo.entity.Product;
import org.springframework.data.domain.Pageable;
import org.springframework.util.MultiValueMap;

import java.util.List;

public interface ProductRepositoryCustom {
    Long countProduct(MultiValueMap<String, String>  queryParams);

    List<Product> searchProduct(MultiValueMap<String, String>  queryParams,  Pageable pageable);
}
