package com.demo.repository.custom.imp;

import com.demo.core.Utils.GetterUtil;
import com.demo.core.Utils.StringUtils;
import com.demo.core.Utils.Validator;
import com.demo.core.constants.Constants;
import com.demo.entity.Product;
import com.demo.repository.custom.ProductRepositoryCustom;
import lombok.extern.slf4j.Slf4j;
import org.springframework.data.domain.Pageable;
import org.springframework.util.MultiValueMap;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import java.time.Instant;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Slf4j
public class ProductRepositoryCustomImpl implements ProductRepositoryCustom {
    @PersistenceContext
    private EntityManager entityManager;

    @Override
    public Long countProduct(MultiValueMap<String, String> queryParams) {
        try {
            StringBuilder sql = new StringBuilder();
            sql.append("SELECT COUNT(p) FROM Product p WHERE 1 = 1 AND p.status = :status ");
            Map<String, Object> values = new HashMap<>();
            sql.append(this.createWhereQuery(queryParams, values));
            Query query = this.entityManager.createQuery(sql.toString(), Product.class);
            query.setParameter("status", Constants.Status.ACTIVE);
            values.forEach(query::setParameter);
            return GetterUtil.getLong(query.getSingleResult(), 0L);
        } catch (Exception exception) {
            log.error(exception.getMessage());
        }
        return 0L;
    }

    @Override
    public List<Product> searchProduct(MultiValueMap<String, String> queryParams, Pageable pageable) {
        try {
            StringBuilder hql = new StringBuilder();
            hql.append("SELECT p FROM Product p WHERE 1 = 1 AND sc.status = :status");
            Map<String, Object> values = new HashMap<>();
            hql.append(this.createWhereQuery(queryParams, values));
            hql.append(this.createOrderQuery());
            Query query = this.entityManager.createQuery(hql.toString(), Product.class);
            query.setParameter("status", Constants.Status.ACTIVE);
            values.forEach(query::setParameter);

            if (Validator.isNotNull(pageable) && pageable.getPageSize() > 0 && pageable.getPageNumber() >= 0) {
                query.setFirstResult(pageable.getPageNumber() * pageable.getPageSize());
                query.setMaxResults(pageable.getPageSize());
            }
            return query.getResultList();
        } catch (Exception e) {
            log.error(e.getMessage());
        }
        return null;
    }

    private StringBuilder createWhereQuery(MultiValueMap<String, String> queryParams, Map<String, Object> values) {
        StringBuilder sql = new StringBuilder(" ");

        System.out.println("line 27");
        System.out.println(queryParams);
        if (queryParams.containsKey("keyword") && !StringUtils.isBlank(queryParams.get("keyword").get(0))) {
            sql.append(" AND ( LOWER(p.productName) LIKE LOWER(:keyword) ");
            sql.append(" OR LOWER(p.subCateName) LIKE LOWER(:keyword) ) ");
            values.put("keyword", "%" + queryParams.get("keyword").get(0).trim() + "%");
        }

        if (queryParams.containsKey("productId") && !StringUtils.isBlank(queryParams.get("productId").get(0))) {
            sql.append(" AND p.id = :productId ");
            values.put("productId", GetterUtil.getLongValue(queryParams.get("productId")));
        }
        if (queryParams.containsKey("startDate") && !StringUtils.isBlank(queryParams.get("startDate").get(0))) {
            sql.append(" AND p.createDate >= :startDate ");
            values.put("startDate", Instant.ofEpochMilli(Long.parseLong(queryParams.get("startDate").get(0))));
        }

        if (queryParams.containsKey("endDate") && !StringUtils.isBlank(queryParams.get("endDate").get(0))) {
            sql.append(" AND p.createDate  <= :endDate ");
            values.put("endDate", Instant.ofEpochMilli(Long.parseLong(queryParams.get("endDate").get(0))));
        }
        return sql;
    }

    private StringBuilder createOrderQuery() {
        StringBuilder sql = new StringBuilder();
        sql.append(" order by p.createDate asc ");
        return sql;
    }
}
