package com.demo.mapper;

import com.demo.controller.protoco.request.ProductRequest;
import com.demo.controller.protoco.response.ProductResponse;
import com.demo.dto.ProductDTO;
import com.demo.entity.Product;
import org.mapstruct.Mapper;

@Mapper(componentModel = "spring")
public interface ProductMapper extends BaseMapper<ProductDTO, Product> {

    // request to entity
    Product requestToEntity(ProductRequest request);

    // DTO to response
    ProductResponse dtoToEntity(ProductDTO dto);

    // Request to DTO
    ProductDTO pathFromRequestToModel( ProductRequest productRequest);



}
