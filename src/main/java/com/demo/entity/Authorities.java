package com.demo.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Entity
@Table(name = "authorities")
public class Authorities extends AbstractAuditingEntity  implements Serializable {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "authrority_id")
	private Long id;

	@Column(name = "authrority_code")
	private String authrotityCode;

	@Column(name = "authroryName")
	private String authroriryName;

//	// cái này là many to many to ROle
//	@Column(name = "role_id")
//	private Long role;
	
	
}
