package com.demo.entity;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.*;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
@Data
@AllArgsConstructor
@NoArgsConstructor
@Entity
@Table(name = "customer")
public class Customer extends AbstractAuditingEntity implements Serializable {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id")
	private Long id;
	
	@Column(name = "username")
	private String username;
	
	@Column(name = "password_hash")
	private String passwordHash;

	@Column(name = "password_slat")
	private String passsowordSlat;
	
	@Column(name = "phone_number")
	private String phoneNumber;
	
	@Column(name = "email")
	private String email;
	
	@Column(name = "address")
	private String address;
	
	@Column(name = "full_name")
	private String fullName;
	
	@Column(name = "gender")
	private Integer gender;
	
	@Column(name = "dob")
	private String dob;
	
	@Column(name = "avatar")
	private String avatar;

	@Column(name = "reward_point")
	private Integer rewardPoint;
	
	@Column(name = "classify")
	private String classify;
	
	@Column(name = "ip_address")
	private String ipAddress;
	
	
}
