package com.demo.entity;

import java.io.Serializable;
import java.sql.Date;

import javax.persistence.Column;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class ProductVoucher  extends AbstractAuditingEntity implements Serializable {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;
	
	@Column(name = "voucher_name")
	private String voucherName;
	
	@Column(name = "voucher_description")
	private String voucherDescription;
	
	@Column(name ="img")
	private String img;
	
	@Column(name = "use_scope")
	private String scope; // 
	
	@Column(name = "active_date")
	private Date activeDate;
	
	@Column(name = "exprice_date")
	private Date expriceDate;
}
