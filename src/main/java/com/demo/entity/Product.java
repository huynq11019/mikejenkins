package com.demo.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
@Data
@AllArgsConstructor
@NoArgsConstructor
@Entity
@Table(name = "products")
public class Product extends AbstractAuditingEntity{
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id")
	private Long id;
	
	@Column(name = "product_name")
	private String productName;
	
	@Column(name = "thumdNail")
	private String thumbNail;
	
	@Column(name="description")
	private String description;
	
	@Column(name = "sub_cate_name")
	private String subCateName;
	
	@Column(name = "price")
	private Long price;
	
	@Column(name = "discount")
	private float discount;
	
	@Column(name= "supplier_id") // người sản xuất
	private Long supplierId;
	
	@Column(name = "vendor_id")
	private Long vendorId; // Nhà cung cấp
}
