package com.demo.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.math.BigDecimal;
import java.util.Date;

import javax.persistence.*;

@Data
@Table(name = "order_detail")
@Entity
@AllArgsConstructor
@NoArgsConstructor
public class OrderDetail extends AbstractAuditingEntity {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id")
	private Long id;

	@Column(name = "orderId")
	private String orderId;
	
	@Column(name = "productId")
	private Integer productId;
	
	@Column(name = "price")
	private BigDecimal price;
	
	@Column(name = "optionId")
	private String optionid;
	
	@Column(name = "quantity")
	private String quantity;
	
	@Column(name = "warrantyTime")
	private Date warranty_time;
	
	
	
	

}
