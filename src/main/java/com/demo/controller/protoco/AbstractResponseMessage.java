package com.demo.controller.protoco;

import com.demo.core.message.ErrorCode;
import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode(callSuper = false)
public abstract class AbstractResponseMessage {
    /** The error code. */
    protected ErrorCode errorCode = ErrorCode.ERR_00000;

    /** The message. */
    protected String message = ErrorCode.ERR_00000.getMessage();

    private Long totalItem;

    /**
     * Sets the error code.
     *
     * @param errorCode
     *            the new error code
     */
    public void setErrorCode(ErrorCode errorCode) {
        this.errorCode = errorCode;
        this.message = errorCode.getMessage();
    }


}
