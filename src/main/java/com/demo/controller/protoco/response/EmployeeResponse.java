package com.demo.controller.protoco.response;

import com.demo.controller.protoco.AbstractResponseMessage;
import com.demo.dto.EmployeeDTO;
import com.demo.entity.Employee;
import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.sql.Date;
import java.util.List;

@JsonInclude(JsonInclude.Include.NON_NULL)
@AllArgsConstructor
@NoArgsConstructor
@Data
@Builder
public class EmployeeResponse  {

    private Long id;

    private String userName;

    private String passwordHash;

    private String email;

    private String address;

    private String fullName;

    private Integer gender;

    private Date dob;

    private String avatar;

    private String role;

    private EmployeeDTO employeeDTO;

    private  List<EmployeeDTO> employeeDTOs;
}
