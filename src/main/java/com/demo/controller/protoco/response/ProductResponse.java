package com.demo.controller.protoco.response;

import com.demo.controller.protoco.AbstractResponseMessage;
import com.demo.dto.ProductDTO;
import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.*;

import java.util.List;

@Data
@Builder
@EqualsAndHashCode(callSuper = false)
@AllArgsConstructor
@NoArgsConstructor
@JsonInclude(JsonInclude.Include.NON_NULL)
public class ProductResponse extends AbstractResponseMessage {
    private String id;

    private String productName;

    private String thumbNail;

    private String description;

    private String subCateName;

    private Long price;

    private float discount;

    private Long supplierId;

    private Long vendorId;

    private ProductDTO productDTO;

    private List<String> images;

    private  List<ProductDTO> listProductDTO;

}
