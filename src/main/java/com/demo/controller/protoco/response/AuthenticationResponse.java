package com.demo.controller.protoco.response;

import com.demo.core.security.JwtToken;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.extern.slf4j.Slf4j;
@Builder
@Data
@AllArgsConstructor
@Slf4j
@NoArgsConstructor
public class AuthenticationResponse  {
    private JwtToken auth = new JwtToken();

    private JwtToken refreshToken= new JwtToken();
//    public void AuthenticationResponse(String Token) {
//        this.token = Token;
//    }
}
