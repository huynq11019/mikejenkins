package com.demo.controller;

import com.demo.controller.protoco.response.ProductResponse;
import com.demo.service.ProductService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.http.ResponseEntity;
import org.springframework.util.MultiValueMap;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@Slf4j
@RestController
@RequestMapping("/api")
public class ProductController {

    @Autowired
    ProductService productService;

    @GetMapping(value = "/product", produces = "application/json")
    public ResponseEntity<ProductResponse> getListPro(@RequestParam MultiValueMap<String, String> queryParams, Pageable pageable) {
        System.out.println(queryParams);
        System.out.println(pageable);
//        ProductResponse response = new ProductResponse();
//        List<ProductDTO> data = productService.searchProduct(queryParams,pageable);
//        response.setListProductDTO(data);
//
////        response.setTotalItem();
        ProductResponse response = productService.searchProduct(queryParams, pageable);
        return ResponseEntity.ok(response);
    }
}
