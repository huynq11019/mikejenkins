package com.demo.service.serviceImpl;

import com.demo.controller.protoco.request.ProductRequest;
import com.demo.controller.protoco.response.ProductResponse;
import com.demo.core.Utils.Validator;
import com.demo.core.constants.Constants;
import com.demo.core.security.SecurityUtils;
import com.demo.dto.ProductDTO;
import com.demo.mapper.ProductMapper;
import com.demo.repository.ProductRepository;
import com.demo.service.ProductService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.util.MultiValueMap;

import javax.transaction.Transactional;
import java.util.ArrayList;
import java.util.List;

@Service
@Transactional
public class ProductServiceImpl implements ProductService {
    @Autowired
    ProductRepository productRepository;

    @Autowired
    ProductMapper productMapper;


    @Override
    public ProductResponse searchProduct(MultiValueMap<String, String> queryParams, Pageable pageable) {
        ProductResponse response = new ProductResponse();
//        Long userId = SecurityUtils.getLoginUserId().orElse(-1L);
        // dem tong so ban ghi
        Long count = this.productRepository.countProduct(queryParams);

        List<ProductDTO> lstDTO = new ArrayList<>();

        // kiểm tra trong db có dữ liệu thì thực hiện query data
        if (Validator.isNotNull(count)) {
            lstDTO = this.productMapper.toDto(productRepository.searchProduct(queryParams, pageable));
        }
        // neu find by id thi set model
        if(Constants.Number.ONE.equals(lstDTO.size())){
            response.setProductDTO(lstDTO.get(0));
        }
        response.setListProductDTO(lstDTO);
        response.setTotalItem(count);
        return response;
    }

    @Override
    public Long countProduct(MultiValueMap<String, String> queryParams) {
        return productRepository.countProduct(queryParams);
    }


    @Override
    public ProductResponse craeteProduct(ProductRequest productRequest) {
        return null;
    }


}
