package com.demo.service.serviceImpl;

import com.demo.controller.protoco.request.EmployeeRequest;
import com.demo.controller.protoco.response.EmployeeResponse;
import com.demo.core.Utils.Validator;
import com.demo.core.message.ErrorCode;
import com.demo.entity.Employee;
import com.demo.mapper.EmployeeMapper;
import com.demo.repository.EmployeeRepository;
import com.demo.service.EmployeeService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.util.MultiValueMap;

import javax.transaction.Transactional;

@Slf4j
@Service
@Transactional
public class EmployeeServiceImpl implements EmployeeService {
    @Autowired
    private EmployeeRepository employeeRepository;
    @Autowired
    private EmployeeMapper employeeMapper;

    @Autowired
    private PasswordEncoder passwordEncoder;
    @Override
    public EmployeeResponse createeEmployee(EmployeeRequest employeeRequest) {
        EmployeeResponse employeeResponse = new EmployeeResponse();
        if (!Validator.isNotNull(employeeResponse.getId())) {
            Employee employee = employeeMapper.requestToEntity(employeeRequest);
            employee.setPasswordHash(passwordEncoder.encode(employee.getPasswordHash()));
            employeeResponse.setEmployeeDTO(employeeMapper.toDto(employeeRepository.save(employee)));
            System.out.println(employeeResponse);

        }
        return employeeResponse;
    }

    @Override
    public EmployeeResponse searchEmployee(MultiValueMap<String, String> queryParams, Pageable pageable) {
        return null;
    }

    @Override
    public EmployeeResponse getById(Long employee) {
        return null;
    }
}
