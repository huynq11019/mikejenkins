package com.demo.service;

import com.demo.controller.protoco.request.ProductRequest;
import com.demo.controller.protoco.response.ProductResponse;
import org.springframework.data.domain.Pageable;
import org.springframework.util.MultiValueMap;


public interface ProductService {
    ProductResponse searchProduct(MultiValueMap<String, String> queryParams, Pageable pageable);

    Long countProduct(MultiValueMap<String, String> queryParams);

//    ProductResponse countSCByApprovalStatus(MultiValueMap<String, String> queryParams);

    ProductResponse craeteProduct(ProductRequest productRequest);
}
