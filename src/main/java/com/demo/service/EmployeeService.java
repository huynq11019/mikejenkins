package com.demo.service;

import com.demo.controller.protoco.request.EmployeeRequest;
import com.demo.controller.protoco.response.EmployeeResponse;
import org.springframework.data.domain.Pageable;
import org.springframework.util.MultiValueMap;

public interface EmployeeService {
     EmployeeResponse createeEmployee(EmployeeRequest employeeRequest);

    public EmployeeResponse searchEmployee(MultiValueMap<String, String> queryParams, Pageable pageable);

    EmployeeResponse getById(Long employee);
}
