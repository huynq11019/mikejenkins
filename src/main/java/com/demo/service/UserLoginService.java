package com.demo.service;

import org.springframework.stereotype.Service;

import com.demo.entity.UserLogin;

public interface UserLoginService {
    /**
     * @param loginLog
     */
    UserLogin save(UserLogin loginLog);

}
